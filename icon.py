#!/usr/bin/env python3

import ping3
import pystray
import threading  # Import the threading module for creating threads
from PIL import Image  # Import the Image module from PIL for creating images


def update_icon():
    while True:
        pass
        # ping_output = ping3.ping("fsf.org", unit="ms")
        # print(f"\n{ping_output:4.0f}", end="")
        # if ping_output == 0:
        #     img = Image.new("RGB", (32, 32), (255, 255, 255))  # 32px32px, white
        # elif 0 < ping_output <= 50:
        #     img = Image.new("RGB", (32, 32), (0, 255, 0))  # 32px32px, green
        # elif 50 < ping_output <= 60:
        #     img = Image.new("RGB", (32, 32), (255, 255, 0))  # 32px32px, yellow
        # elif 60 < ping_output < 100:
        #     img = Image.new("RGB", (32, 32), (255, 165, 0))  # 32px32px, orange
        # elif ping_output >= 100:
        #     img = Image.new("RGB", (32, 32), (255, 0, 0))  # 32px32px, red
        # icon.icon = img


if __name__ == "__main__":
    icon = pystray.Icon("ping")
    icon.icon = Image.new("RGB", (32, 32), (0, 0, 0))  # 32px32px, white
    # Create a new thread to run the update_icon() function
    thread = threading.Thread(target=update_icon)
    # Start the thread
    thread.start()
    icon.run()

# image = Image.open("/home/user/my-signature.png")


# def after_click(icon, query):
#     if str(query) == "":
#         print(
#             "The Best Place to learn anything Tech \
#         Related -> https://www.geeksforgeeks.org/"
#         )
#         # icon.stop()
#     elif str(query) == "Exit":
#         icon.stop()


# icon = pystray.Icon(
#     "GFG",
#     image,
#     "GeeksforGeeks",
#     menu=pystray.Menu(
#         pystray.MenuItem("GeeksforGeeks Website", after_click),
#         pystray.MenuItem("GeeksforGeeks Youtube", after_click),
#         pystray.MenuItem("GeeksforGeeks LinkedIn", after_click),
#         pystray.MenuItem("Exit", after_click),
#     ),
# )

# icon.run()
